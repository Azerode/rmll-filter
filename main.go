package main

import (
	"database/sql"
	"deltaKehKeh/src"
	"log"
	"os"

	_ "github.com/mattn/go-sqlite3"
)

func main() {
	var information interface{}

	db, err := sql.Open("sqlite3", "./database/data.db")

	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	if len(os.Args) > 1 && os.Args[1] == "create" {
		src.CreateDatabase(db)
		information = src.GetData()

		src.FillDatabase(information, db)
	}

	src.YmlConverter(db)
}
