package src

import (
	"database/sql"
	"log"
	"reflect"
)

// CreateDatabase create tables
func CreateDatabase(db *sql.DB) {
	_, err := db.Exec(`CREATE TABLE IF NOT EXISTS Author(
		IdAuthor INTEGER PRIMARY KEY AUTOINCREMENT,
		Name TEXT)`)

	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`CREATE TABLE IF NOT EXISTS Room(
		IdRoom INTEGER PRIMARY KEY AUTOINCREMENT,
		Name TEXT)`)

	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`CREATE TABLE IF NOT EXISTS Event(
		IdEvent INTEGER PRIMARY KEY AUTOINCREMENT,
		Title TEXT,
		Description TEXT,
		Length INTEGER,
		Type INTEGER)`)

	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(` CREATE TABLE IF NOT EXISTS Subject(
		IdSubject INTEGER PRIMARY KEY AUTOINCREMENT,
		Title TEXT NOT NULL,
		Description TEXT NOT NULL,
		Author INTERGER NOT NULL,
		Room INTEGER NOT NULL,
		Date TIMESTAMP NOT NULL,
		EventType INTEGER NOT NULL,
		FOREIGN KEY(Author) REFERENCES Author(IdAuthor)
		FOREIGN KEY(Room) REFERENCES Room(IdRoom)
		FOREIGN KEY(EventType) REFERENCES Event(Id)
		)`)

	if err != nil {
		log.Fatal(err)
	}
}

// FillDatabase fill the database with data
func FillDatabase(information interface{}, db *sql.DB) {

	// Contient l'élément rooms des données
	rooms := reflect.ValueOf(reflect.ValueOf(information).Index(0).Interface().(map[string]interface{}))
	tabRooms := rooms.MapIndex(reflect.ValueOf("rooms")).Interface().([]interface{})
	tabEventTypes := rooms.MapIndex(reflect.ValueOf("event_types")).Interface().([]interface{})

	for _, eventType := range tabEventTypes {

		info := reflect.ValueOf(eventType).Interface().(map[string]interface{})
		_, err := db.Exec(`INSERT INTO Event(Title,Description,Length,Type) VALUES($1,$2,$3,$4)`,
			info["title"].(string), info["description"].(string), info["length"].(float64), info["id"].(float64))

		if err != nil {
			log.Fatal(err)
		}
	}

	for numRoom, room := range tabRooms {

		// Contient l'élément event
		events := reflect.ValueOf(room).Interface().(map[string]interface{})["events"].([]interface{})

		for _, event := range events {
			var idEvent float64
			info := reflect.ValueOf(event).Interface().(map[string]interface{})

			resAuthor, err := db.Exec("INSERT INTO Author(Name) VALUES ($1)", info["speaker_names"].(string))
			if err != nil {
				log.Fatal(err)
			}

			resRoom, err := db.Exec("INSERT INTO Room(Name) VALUES ($1)", numRoom)
			if err != nil {
				log.Fatal(err)
			}

			idAuthor, err := resAuthor.LastInsertId()
			if err != nil {
				log.Fatal(err)
			}

			idRoom, err := resRoom.LastInsertId()
			if err != nil {
				log.Fatal(err)
			}

			rows, err := db.Query(`SELECT IdEvent FROM Event WHERE Type = ($1)`, info["event_type_id"].(float64))

			if err != nil {
				log.Fatal(err)
			}
			for rows.Next() {
				rows.Scan(&idEvent)
			}

			_, err = db.Exec(`INSERT INTO Subject(Title,Description,Author,Room,Date, EventType)
			 VALUES ($1,$2,$3,$4,$5,$6)`, info["title"].(string), info["abstract"].(string), idAuthor, idRoom, info["start_time"].(string), idEvent)
			if err != nil {
				log.Fatal(err)
			}
		}
	}
}
