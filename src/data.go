package src

import (
	"database/sql"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"gopkg.in/yaml.v2"
)

// Rooms : Data form for the database
type Rooms struct {
	Author      string
	Title       string
	Description string
	From        string
	To          string
	Place       string
	Twitter     string
	URL         string
	Date        string
}

// GetData get the data since a webservice
func GetData() interface{} {
	var information interface{}
	response, err := http.Get("https://2018.rmll.info/api/v1/conferences/rmll2018/")

	if err != nil {
		log.Fatal(err)
	}
	defer response.Body.Close()

	body, err := ioutil.ReadAll(response.Body)

	if err != nil {
		log.Fatal(err)
	}
	json.Unmarshal(body, &information)

	return information
}

// YmlConverter convert data in yml file
func YmlConverter(db *sql.DB) {
	data := make(map[string]map[string][]Rooms)

	rows, err := db.Query(`SELECT a.Name, s.Title, s.Description, r.Name, date(s.Date), strftime('%H:%M', s.Date), strftime('%H:%M', Datetime(s.Date,'+'|| e.Length || ' minutes')) 
	FROM Subject AS s 
	LEFT JOIN Event AS e ON e.IdEvent = s.EventType
	LEFT JOIN Author AS a ON s.Author = a.IdAuthor 
	LEFT JOIN Room AS r ON s.Room = r.IdRoom 
	GROUP BY s.Date`)
	if err != nil {
		log.Fatal(err)
	}

	defer rows.Close()

	for rows.Next() {
		var info Rooms
		var tabRooms []Rooms

		rows.Scan(&info.Author, &info.Title, &info.Description, &info.Place, &info.Date, &info.From, &info.To)
		tabRooms = append(tabRooms, info)

		mm, ok := data[info.Date]
		if !ok {
			mm = make(map[string][]Rooms)
			data[info.Date] = mm
		}
		mm["talks"] = append(data[info.Date]["talks"], info)
	}

	yml, err := yaml.Marshal(&data)

	if err != nil {
		log.Fatal(err)
	}

	ioutil.WriteFile("2018.yml", yml, 0644)
}
